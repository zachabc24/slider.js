var Slider = function(conf){
  "use strict";
  
  function VerticalSlider(conf){
    var container;
    
    var canvas;
    var context;
    var sCanvas;
    var sContext;
    
    var incs;
    var notches;
    var current;
    var buffer;
    var style;
    
    var click;
    var drag;

    var addEventListeners = function(){
      sCanvas.addEventListener('mousedown', mousepushdown, false);
      sCanvas.addEventListener('touchstart', touchpushdown, false);
    };

    var touchpushdown = function(e){
      var y = e.targetTouches[0].pageY - sCanvas.getBoundingClientRect().top;
      
      var sp = (sCanvas.height -2*buffer)  / (incs-1);
      var n =  Math.round(Math.min(Math.max((y-buffer) / sp, 0),incs-1));

      if(current !== notches[n]){
        current = notches[n];
        if(click) {click({notch: n});}
      }

      sContext.clearRect(0,0, sCanvas.width, sCanvas.height); 
      drawFill();
      drawSlider();

      //for dragging and dropping
      sCanvas.addEventListener("touchmove", touchdrag, false);
      sCanvas.addEventListener("touchend", touchgone, false);

      e.preventDefault();
    };

    var touchdrag = function(e){
      var y = e.targetTouches[0].pageY - sCanvas.getBoundingClientRect().top;

      var sp = (sCanvas.height -2*buffer)  / (incs-1);
      var n =  Math.round(Math.min(Math.max((y-buffer) / sp, 0),incs-1));

      if(current !== notches[n]){
        current = notches[n];
        if(click){ click({notch: n});}
      }

      sContext.clearRect(0,0, sCanvas.width, sCanvas.height); 
      drawFill();
      drawSlider();
    };

    var touchgone = function(e){
      sCanvas.removeEventListener("touchmove", touchdrag, false);
      sCanvas.addEventListener("touchend", touchgone, false);
    };
    
    var mousepushdown = function(e){
      var y = e.pageY - sCanvas.getBoundingClientRect().top;

      var sp = (sCanvas.height -2*buffer)  / (incs-1);
      var n =  Math.round(Math.min(Math.max((y-buffer) / sp, 0),incs-1));

      if(current !== notches[n]){
        current = notches[n];
        if(click){click({notch: n});}
      }

      sContext.clearRect(0,0, sCanvas.width, sCanvas.height); 
      drawFill();
      drawSlider();

      //for dragging and dropping
      sCanvas.addEventListener("mousemove", mousedrag, false);
      sCanvas.addEventListener("mouseout", mousegone, false);
      sCanvas.addEventListener("mouseup", mouseraise, false);

      e.preventDefault();
    };

    var mousedrag = function(e){
      var y = e.pageY - sCanvas.getBoundingClientRect().top;

      var sp = (sCanvas.height -2*buffer)  / (incs-1);
      var n =  Math.round(Math.min(Math.max((y-buffer) / sp, 0),incs-1));

      if(current !== notches[n]){
        current = notches[n];
        if(click){click({notch: n});}
      }

      sContext.clearRect(0,0, sCanvas.width, sCanvas.height); 
      drawFill();
      drawSlider();
    };

    var mouseraise = function(e){
      sCanvas.removeEventListener("mousemove", mousedrag, false);
      sCanvas.addEventListener("mouseout", mousegone, false);
      sCanvas.addEventListener("mouseup", mouseraise, false);
    };
    var mousegone = function(e){
      sCanvas.removeEventListener("mousemove", mousedrag, false);
      sCanvas.addEventListener("mouseout", mousegone, false);
      sCanvas.addEventListener("mouseup", mouseraise, false);
    };

    var buildslider = function(){
      canvas = document.createElement('canvas');
      context = canvas.getContext('2d');
      canvas.height = parseInt(container.style.height, 10);
      canvas.width = style.slidewidth;
      canvas.style.position = "absolute";
      canvas.style.left = 0;

      drawNotches();
      drawGroove();

      sCanvas = document.createElement('canvas');
      sContext = sCanvas.getContext('2d');
      sCanvas.height = parseInt(container.style.height, 10);
      sCanvas.style.position = "absolute";
      sCanvas.style.left = 0;
      sCanvas.style.cursor = "pointer";
      sCanvas.width = style.slidewidth;

      current = notches[style.start];
      drawFill();
      drawSlider();

      container.appendChild(canvas);
      container.appendChild(sCanvas);
    };

    var drawGroove = function(){
      context.save();
      context.fillStyle = style.groovecolor;
      context.strokeStyle = style.groovecolor;
      var w = style.rad/2;

      context.beginPath();
      context.lineWidth = 2*w;
      context.moveTo(canvas.width/2, w);
      context.lineTo(canvas.width/2, canvas.height-w);
      context.stroke();
      context.closePath();
      
      context.beginPath();
      context.lineWidth = 1;
      context.arc(canvas.width/2,w,w,0,2*Math.PI);
      context.stroke();
      context.fill();
      context.closePath();

      context.beginPath();
      context.lineWidth = 1;
      context.arc(canvas.width/2,canvas.height-w,w,0,2*Math.PI);
      context.stroke();
      context.fill();
      context.closePath();

      context.restore();
    };

    var drawNotches = function(){
      context.save();

      var sp = (canvas.height -2*buffer)  / (incs-1);
      var margin = conf.margin || 5;
      var notchHeight = 1;

      for(var i = 0; i < incs; i++){
        var notch = {position: {y: i*sp+buffer}};
        notches.push(notch);
        if(style.shownotches){
          context.fillRect(margin, notch.position.y - notchHeight/2, canvas.width-2*margin, notchHeight);
        }
      }

      context.restore();
    };

    var drawFill = function(){
      sContext.save();
      sContext.strokeStyle = style.fillcolor;
      sContext.fillStyle = style.fillcolor;
      drawFillToPosition(current.position.y - style.rad);
      sContext.restore();
    };

    var drawFillToPosition = function(y){
      var w = style.rad/2; 

      sContext.beginPath();
      sContext.lineWidth = 2*w;
      sContext.moveTo(canvas.width/2, w);
      sContext.lineTo(canvas.width/2, y);
      sContext.stroke();
      sContext.closePath();
      
      sContext.beginPath();
      sContext.lineWidth = 1;
      sContext.arc(canvas.width/2,w,w,0,2*Math.PI);
      sContext.stroke();
      sContext.fill();
      sContext.closePath();
    };

    var drawSlider = function(){
      sContext.save();
      sContext.fillStyle = style.slidefillcolor;
      sContext.strokeStyle = style.slidestrokecolor;
      drawSliderAtPosition(sCanvas.width/2, current.position.y);
      sContext.restore();
    };
    var drawSliderAtPosition = function(x,y){
      sContext.beginPath();
      sContext.lineWidth = style.rad/2; 
      sContext.arc(x,y,style.rad,0,2*Math.PI);
      sContext.stroke();
      sContext.fill();
      sContext.closePath();
    };
    var onclick = function(callback){
      click = callback;
      return this;
    };

    var ondrag = function(callback){
      drag = callback;
      return this;
    };

    var init = function(){
     
      incs = conf.notches ? Math.min(conf.notches, 100) : 100;
      buffer = conf.buffer || 10;
      notches = [];
      current = conf.current || 0;
      style = {};
      style.fillcolor = conf.fillcolor || "black";
      style.slidefillcolor = conf.slidefillcolor || "gray";
      style.slidestrokecolor = conf.slidestrokecolor || "black";
      style.groovecolor = conf.groovecolor || "gray";
      style.shownotches = conf.shownotches || false;
      style.start = Math.min(conf.start, incs) || 0;
      style.rad = conf.radius || 8;
      style.slidewidth = conf.slidewidth || parseInt(conf.container.style.width, 10) || 30;

      container = document.createElement('div');
      container.style.height = conf.container.style.height;
      container.style.width = style.slidewidth;
      container.style.position = "relative";
      conf.container.appendChild(container);

      
      buildslider();
      addEventListeners();
    };
    init();

    return{
      onclick: onclick,
      ondrag: ondrag
    };

  }
  return function(conf){
      var vs = new VerticalSlider(conf);
      return vs;
  }(conf);
   
};

